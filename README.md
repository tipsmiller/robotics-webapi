# Robotics WebAPI #

This is a Java project created with the goal to provide web services consumable by robots.

### Overview ###

This project will provide services which would normally be hard to implement on small, cheap robots by offloading the processing to a server. The project is configured as a Java container to be deployed to a web server.

### What's been implemented? ###

- TennisProcessor is a stateless service to locate a tennis ball in an image.

