package com.miller.robots.RoboticsAPI;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import org.opencv.core.*;
import org.opencv.imgcodecs.*;

@Path("/tennis")
public class TennisProcessor {
	
	long time1, time2, time3;
	
	public TennisProcessor() {
	}
	
	@GET
	@Path("/status")
	public String GetEndpointStatus() {
		return "Online";
	}
	
	@POST
	@Path("/distancecenter")
	@Consumes("image/jpeg")
	public String GetDistanceAndPosition(InputStream image) throws IOException {
		Mat inputImage = readInputStreamIntoMat(image);
		return inputImage.toString();
	}
	
	
	//from: http://answers.opencv.org/question/31855/java-api-loading-image-from-any-java-inputstream/?answer=39590#post-id-39590
	private Mat readInputStreamIntoMat(InputStream inputStream) throws IOException {
		time1 = System.nanoTime();
	    // Read into byte-array
	    byte[] temporaryImageInMemory = readStream(inputStream);
		time2 = System.nanoTime();

	    // Decode into mat. Use any IMREAD_ option that describes your image appropriately
	    Mat outputImage = Imgcodecs.imdecode(new MatOfByte(temporaryImageInMemory), Imgcodecs.IMREAD_COLOR);
		time3 = System.nanoTime();
		System.out.println("Times (ms): " + (time2-time1)/1000000 + ", " + (time3-time2)/1000000);
	    return outputImage;
	}
	
	//from: http://answers.opencv.org/question/31855/java-api-loading-image-from-any-java-inputstream/?answer=39590#post-id-39590
	private byte[] readStream(InputStream stream) throws IOException {
	    // Copy content of the image to byte-array
	    ByteArrayOutputStream buffer = new ByteArrayOutputStream();
	    int nRead;
	    byte[] data = new byte[9999999];

	    while ((nRead = stream.read(data, 0, data.length)) != -1) {
	        buffer.write(data, 0, nRead);
	    }

	    buffer.flush();
	    System.out.println("Buffer length: " + buffer.size());
	    byte[] temporaryImageInMemory = buffer.toByteArray();
	    buffer.close();
	    stream.close();
	    return temporaryImageInMemory;
	}
}
