package com.miller.robots.RoboticsAPI;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.opencv.core.Core;

public class OpenCVStartup implements ServletContextListener {

	@Override
	public void contextDestroyed(ServletContextEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void contextInitialized(ServletContextEvent arg0) {
		// TODO Auto-generated method stub
		System.out.println("OpenCVStartup: Loading OPENCV library");
		System.loadLibrary( Core.NATIVE_LIBRARY_NAME );
	}

}
