package com.miller.robots.RoboticsAPI;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import org.opencv.core.*;

@Path("hellocv")
public class HelloCV {
	
	@GET
	public String GetThis() {
		System.loadLibrary( Core.NATIVE_LIBRARY_NAME );
        Mat mat = Mat.eye( 3, 3, CvType.CV_8UC1 );
		return "<h1>" + mat.dump() + "</h1>";
	}
	
	@POST
	public String GetThat() {
		System.loadLibrary( Core.NATIVE_LIBRARY_NAME );
        Mat mat = Mat.eye( 3, 3, CvType.CV_8UC1 );
		return "<h1>" + mat.dump() + "</h1>";
	}
}
